var indexSectionsWithContent =
{
  0: "_abcdefghijklmnoprstuvx~",
  1: "abcdefghiklmnoprstuv",
  2: "mo",
  3: "abcdefghiklmnpstuv",
  4: "_abcdfghilmnoprstuv~",
  5: "abcdefgijmorstvx",
  6: "cdefmnrs",
  7: "acfmr",
  8: "bhlmnt",
  9: "acop",
  10: "_bcdmn",
  11: "abcdefghlmnprstuv",
  12: "abcfgimntu"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "related",
  10: "defines",
  11: "groups",
  12: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Friends",
  10: "Macros",
  11: "Modules",
  12: "Pages"
};

